# Development

## create `.env`


configure oauth provider
```
CLIENT_ID=
CLIENT_SECRET=
AUTH_URL=https://808ecab812ec.ngrok.io/callback


configure slack
```
SLACK_CHANNEL_ID=
SLACK_SIGNING_SECRET=
SLACK_BOT_TOKEN=
```

## Launching Local Development Server

launch development server with:

```
docker-compose up -d
```

navigate to [localhost:4551](http://localhost:4551/) to view the
domain created by [ngrok](https://ngrok.com/).


## Configure Slackbot


navigate to your [app management
dashboard](https://api.slack.com/apps) on slack, and slack the bot
application that is being developed. In the **Features** sidebar, select
`OAuth & Permissions` and set the `Bot User OAuth Token` in your `.env`

```text
SLACK_BOT_TOKEN=xoxb-XXXXXXXXX-XXXXXXXXXX-XXXXXXXXXXXXXXXXXXXX
```

also configure the [channel id](https://stackoverflow.com/questions/40940327/what-is-the-simplest-way-to-find-a-slack-team-id-and-a-channel-id)
to post messages too

```text
SLACK_CHANNEL_ID=024FRLAG4X
```

## Configure DynamoDB

https://docs.aws.amazon.com/sdk-for-go/api/service/dynamodb/ 

create an aws user with the following iam permissions
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "dynamodb:*",
            "Resource": "*"
        }
    ]
}
```
and configure environment variables

```
AWS_ACCESS_KEY_ID=XXXXXXXXXXXXXXXxxxxx
AWS_SECRET_ACCESS_KEY=xxxxxxxxxxxxxxxxxxxxxxxxxxxx
AWS_REGION="us-east-1"
```

## restart ngrok
```shell
$ docker-compose restart ngrok
```

## rebuild and restart app

```shell
$ docker-compose up --build app
```

## print logs for app

```shell
$ docker-compose logs -tf app

```
