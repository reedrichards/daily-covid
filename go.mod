module dailycovid

go 1.15

require (
	github.com/joho/godotenv v1.3.0
	github.com/slack-go/slack v0.9.3
	golang.org/x/oauth2 v0.0.0-20210628180205-a41e5a781914
)
