package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path"

	_ "github.com/joho/godotenv/autoload"
	"github.com/slack-go/slack"
)

var (
	SLACK_CHANNEL_ID = os.Getenv("SLACK_CHANNEL_ID")
	SLACK_BOT_TOKEN  = os.Getenv("SLACK_BOT_TOKEN")
	IMAGE_URL        = flag.String("url", "", "url of image to post on slack")
)

func sendMessageToChannel(msg string, api *slack.Client) error {
	_, _, err := api.PostMessage(
		SLACK_CHANNEL_ID,
		slack.MsgOptionText(msg, false),
		slack.MsgOptionAsUser(true), // Add this if you want that the bot would post message as a user, otherwise it will send response using the default slackbot
	)
	return err
}

func main() {
	flag.Parse()
	api := slack.New(SLACK_BOT_TOKEN)
	dir := "test_dir"

	os.Mkdir(dir, 0777)

	fileName := path.Join(dir, "img.jpg")
	out, err := os.Create(fileName)
	if err != nil {
		log.Fatal(err)
	}
	defer out.Close()
	resp, err := http.Get(*IMAGE_URL)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	params := slack.FileUploadParameters{
		Title:    "Screenshot of " + *IMAGE_URL,
		Filetype: "jpg",
		File:     fileName,
		//Content:  "Nan Nan Nan Nan Nan Nan Nan Nan Batman",
		Channels: []string{SLACK_CHANNEL_ID},
	}
	file, err := api.UploadFile(params)
	if err != nil {
		fmt.Printf("%s\n", err)
		return
	}
	fmt.Printf("Name: %s, URL: %s\n", file.Name, file.URL)

}
