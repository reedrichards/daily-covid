resource "aws_ecr_repository" "dailycovid" {
  name                 = "dailycovid"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}
